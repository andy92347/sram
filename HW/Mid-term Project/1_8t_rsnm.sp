**mid_8t_1v_snm**
.inc "D:\programs\synopsys\65nm_bulk.pm"

.param Vval=0.6v

Vvdd vdd 0 dc Vval

VBL  BL 0 dc 0v
VBLB BLB 0 dc 0v
VWL  WL 0 dc 0v

VRBL RBL 0 dc Vval 
VRWL RWL 0 dc Vval

VQ1 Q1 0 DC Vval
VQB2 QB2 0 DC 0V

M1  Qb1 Q1 vdd vdd pmos  w=0.54u l=0.18u
M2  Qb1 Q1 0 0 nmos  w=0.18u l=0.18u

M3  Q2 QB2 vdd  vdd pmos  w=0.54u l=0.18u
M4  Q2 QB2  0 0 nmos  w=0.18u l=0.18u

M5 BLB WL  Qb1  0 nmos  w=0.45u l=0.18u
M6 BL  WL  Q2 0 nmos  w=0.45u l=0.18u

M8 RBL RWL pass pass nMOS w=0.45u l=0.18u
M9 pass Qb1 0 0 nMOS w=0.45u l=0.18u

.dc VQ1 0V  Vval  0.01V
.print dc v(Qb1) 

.dc VQB2 0V  Vval  0.01V
.print dc v(Q2)

.option post=1
.option INGOLD=2
.end