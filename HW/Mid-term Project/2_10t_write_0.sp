**mid_6t**
.inc "D:\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 0.8v
V1 BL Gnd PWL  0.5n 0.8v,0.51n 0v,2.51n 0v,2.52n 0.8v
V2 BLB Gnd PWL 0.5n 0.8v,0.51n 0.8v,2.51n 0.8v,2.52n 0.8v
V3 WL Gnd PWL  0.5n 0v,0.51n 0.8v,2.51n 0.8v,2.52n 0v

**MP nd ng ns <nb> name w l
M1 Qb Q Vdd Vdd pMOS w=1u l=0.18u
M2 Qb Q node0 node0 nMOS w=2u l=0.18u
M7 node0 Q Gnd Gnd nMOS w=2u l=0.18u

M3 Q Qb Vdd Vdd pMOS w=1u l=0.18u
M4 Q Qb node1 node1 nMOS w=2u l=0.18u
M8 node1 Qb Gnd Gnd nMOS w=2u l=0.18u

**M5 Qb WL BLB BLB nMOS w=1u l=0.18u
**M6 Q WL BL BL nMOS w=1u l=0.18u
M5 BLB WL Qb Qb nMOS w=1u l=0.18u
M6 BL WL Q Q nMOS w=1u l=0.18u

M9 Vdd Qb node0 node0 nMOS w=2u l=0.18u
M10 Vdd Q node1 node1 nMOS w=2u l=0.18u

.ic V(Q)=0.8v
.ic V(Qb)=0v
**.ic V(WL)=0v


.tran 0.1ns 4ns
.option post=1
.temp 25

.end