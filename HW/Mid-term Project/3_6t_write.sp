**mid_6t**
.inc "D:\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 1v
V1 BL Gnd PWL  2n 1v,4n 1v,6n 1v,8n 1v
V2 BLB Gnd PWL 2n 1v,4n 0v,6n 0v,8n 1v
V3 WL Gnd PWL  2n 0v,4n 1v,6n 1v,8n 0v


**MP nd ng ns <nb> name w l
M1 Qb Q Vdd Vdd pMOS w=1u l=0.18u
M2 Qb Q Gnd Gnd nMOS w=2u l=0.18u

M3 Q Qb Vdd Vdd pMOS w=1u l=0.18u
M4 Q Qb Gnd Gnd nMOS w=2u l=0.18u

**M5 Qb WL BLB BLB nMOS w=1u l=0.18u
**M6 Q WL BL BL nMOS w=1u l=0.18u
M5 BLB WL Qb Qb nMOS w=1u l=0.18u
M6 BL WL Q Q nMOS w=1u l=0.18u

.ic V(Q)=0v
.ic V(Qb)=1v



.tran 0.1ns 10ns
.option post=1
.temp 25

.end