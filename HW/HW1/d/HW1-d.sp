**HW1-d**
.inc "D:\synopsys\65nm_bulk.pm"
Vdd Vdd Gnd dc 1V

MP Vout Vin Vdd Vdd pMOS w=1u l=0.18u
MN Vout Vin Gnd Gnd nMOS w=1u l=0.18u

.dc Vin 0V 1V 0.05V
.meas dc power PAR='(V(Vout)*I(Vdd))'
.option post=1
.temp 25

.end
