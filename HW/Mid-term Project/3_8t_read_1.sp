**mid_6t**
.inc "D:\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 0.8v
**write
*V1 BL Gnd PWL  2n 1v,4n 1v,6n 1v,8n 1v
*V2 BLB Gnd PWL 2n 1v,4n 0v,6n 0v,8n 1v
*V3 WL Gnd PWL  2n 0v,4n 1v,6n 1v,8n 0v

**read
**V1 BL Gnd PWL  2n 1v,4n 1v,6n 1v,8n 1v
**V2 BLB Gnd PWL 2n 1v,4n 0v,6n 0v,8n 1v
*V3 WL Gnd PWL  0.5n 0v,0.51n 0.8v,2.51n 0.8v,2.52n 0v
V4 RWL Gnd PWL  0.5n 0v,0.51n 0.8v,2.51n 0.8v,2.52n 0v

**MP nd ng ns <nb> name w l
M1 Qb Q Vdd Vdd pMOS w=1u l=0.18u
M2 Qb Q Gnd Gnd nMOS w=2u l=0.18u
M3 Q Qb Vdd Vdd pMOS w=1u l=0.18u
M4 Q Qb Gnd Gnd nMOS w=2u l=0.18u

**M5 Qb WL BLB BLB nMOS w=1u l=0.18u
**M6 Q WL BL BL nMOS w=1u l=0.18u
M5 BLB WL Qb Qb nMOS w=1u l=0.18u
M6 BL WL Q Q nMOS w=1u l=0.18u

M7 RBL RWL Con Con nMOS w=2u l=0.5u
M8 Con Qb Gnd Gnd nMOS w=2u l=0.18u


.ic V(Q)=0.8v
.ic V(Qb)=0v
*.ic V(BL)=0.8v
*.ic V(BLB)=0.8v
.ic V(RBL)=0.8v

.plot I1(M7)

.tran 0.05ns 4.5ns
.option post=1
.temp 25

.end