**HW2-a**
.inc "D:\programs\synopsys\65nm_bulk.pm"

.global Vdd Gnd
Vdd Vdd Gnd dc 1V
Vin Vin Gnd pulse 0V 1V 0ns 0.1ns 0.1ns 0.4ns 0

.subckt inv In Out
MP Out In Vdd Vdd pMOS w=1u l=0.18u
MN Out In Gnd Gnd nMOS w=1u l=0.18u
.ends inv

x1 Vin net1 inv
x2 net1 net2 inv
x3 net2 net3 inv
x4 net3	net4 inv
x5 net4	net5 inv
x6 net5	net6 inv
x7 net6	net7 inv
x8 net7	net8 inv
x9 net8	net9 inv
x10 net9 net10 inv
x11 net10 net11 inv
x12 net11 net12 inv
x13 net12 net13 inv
x14 net13 net14 inv
x15 net14 net15 inv
x16 net15 net16 inv
x17 net16 net17 inv
x18 net17 net18 inv
x19 net18 net19 inv
x20 net19 net20 inv
x21 net20 net21 inv
x22 net21 net22 inv
x23 net22 net23 inv
x24 net23 net24 inv
x25 net24 net25 inv
x26 net25 net26 inv
x27 net26 net27 inv
x28 net27 net28 inv
x29 net28 net29 inv
x30 net29 net30 inv
x31 net30 net31 inv
x32 net31 net32 inv
x33 net32 net33 inv
x34 net33 net34 inv
x35 net34 net35 inv
x36 net35 net36 inv
x37 net36 net37 inv
x38 net37 net38 inv
x39 net38 net39 inv
x40 net39 net40 inv
x41 net40 net41 inv
x42 net41 net42 inv
x43 net42 net43 inv
x44 net43 net44 inv
x45 net44 net45 inv
x46 net45 net46 inv
x47 net46 net47 inv
x48 net47 net48 inv
x49 net48 net49 inv
x50 net49 net50 inv
x51 net50 net51 inv
x52 net51 net52 inv
x53 net52 net53 inv
x54 net53 net54 inv
x55 net54 net55 inv
x56 net55 net56 inv
x57 net56 net57 inv
x58 net57 net58 inv
x59 net58 net59 inv
x60 net59 net60 inv
x61 net60 net61 inv
x62 net61 net62 inv
x63 net62 net63 inv
x64 net63 net64 inv

.CONNECT Vout4 net4
.CONNECT Vout8 net8
.CONNECT Vout16 net16
.CONNECT Vout32 net32
.CONNECT Vout64 net64

.tran 0.1ns 15ns
.option post=1
.temp 25

.end